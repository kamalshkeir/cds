module github.com/ovh/cds/tools/smtpmock

go 1.17

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fsamin/smtp v0.0.0-20190904085838-54a7c428d4f9
	github.com/labstack/echo v3.3.10+incompatible
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0
	github.com/urfave/cli/v2 v2.1.1
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0-20190314233015-f79a8a8ca69d // indirect
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/russross/blackfriday/v2 v2.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.0.1 // indirect
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472 // indirect
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3 // indirect
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
